//
// Created by mikaj on 30.03.2023.
//
#include <stdio.h>

unsigned int option = 0;
unsigned int amount = 0;
float coin = 0;
float money = 0;
float costs = 0;
float mreturn = 0;
float  saldo = 0;

int main() {

    printf("Getraenkeautomat");
    printf("Folgende Getraenke haben wir im Angebot\n");
    printf("1: Wasser (0.5 CHF)\n");
    printf("2: Sprudelwasser (1.0 CHF)\n");
    printf("3: Vitaminwasser (1.5 CHF)\n");
    printf("4: Cola (2.0CHF)\n");
    printf("\n");
    printf("Geben sie 1, 2, 3 oder 4 ein: ");
    scanf("%d", &option);
    printf("Wie viele Falschen moechten Sie? ");
    scanf("%d", &amount);
    printf("\e[1;1H\e[2J"); //Clean Terminal

    switch (option) {
        case 1:
            costs = 0.5 * amount;
            break;
        case 2:

            costs = 1.0 * amount;
            break;
        case 3:

            costs = 1.5 * amount;
            break;
        case 4:

            costs = 2.0 * amount;
            break;

        default:
            printf("Invalid input please try again.");
    }
    printf("--- Bezahlvorgang ---\n");

    while (costs != money){

        saldo = costs-money;
        if(saldo < 0){

            mreturn = saldo*-1;
            break;
        }
        printf("Der offen Betrag ist %f bitte Werfen Sie eine Muenze ein: ", saldo);
        scanf("%f", &coin);


        if (coin >= 0.5){

            printf("Es wurde eine %f CHF Muenze eingeworfen.\n", coin);
            money = money+coin;
            coin = 0;
        }
        else{
            printf("Es wurde keine Muenze eingeworfen.\n");
        }
    }
    printf ("--- Getraenkeausgabe ---\n");

    for (int i = 1; i <= amount; ++i) {

        printf("Flasche %d von %d wurde ausgegeben\n", i,amount);
        printf("\n");

    }

    printf ("--- Wechselgeldausgabe ---\n");
    printf("bitte entnehmen Sie ihr Wechselgeld von %f",mreturn);
    return 0;
    }