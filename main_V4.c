//
// Created by mikaj on 13.04.2023.
//
#include <stdio.h>

unsigned int sorteauswahl();
unsigned int calculationamount();
float calculationsaldo(unsigned int sorte, unsigned int amount);
float paing (float preis);
void output (unsigned int sorte, unsigned int amount, float mreturn);
int verifycoin(float coin);


int main(){

    printf("Getraenkeautomat_V4");
    unsigned int amount, sorte;
    float preis,mreturn;

    sorte = sorteauswahl();
    amount = calculationamount();
    preis = calculationsaldo(sorte, amount);
    mreturn = paing(preis);
    output(sorte, amount, mreturn);

    return 0;
}

unsigned int sorteauswahl(){

    unsigned int sorte=0;
    printf("Folgende Getraenke haben wir im Angebot\n");
    printf("1: Wasser (0.5 CHF)\n");
    printf("2: Sprudelwasser (1.0 CHF)\n");
    printf("3: Vitaminwasser (1.5 CHF)\n");
    printf("4: Cola (2.0CHF)\n");
    printf("\n");
    printf("Geben sie 1, 2, 3 oder 4 ein: ");
    scanf("%d", &sorte);
    if(sorte > 4){
        printf("Invalid input please try again.");
        sorteauswahl();
    }

    return sorte;

}

unsigned int calculationamount(){

    unsigned int amount;
    printf("Wie viele Falschen moechten Sie (Max: 15)? ");
    scanf("%d", &amount);
    if(amount > 15){

        printf("Invalid input please try again.\n");
        calculationamount();
    }

    return amount;

}

float calculationsaldo(unsigned int sorte, unsigned int amount){

    float preis = 0;

    switch (sorte) {

        case 1:
            preis = 0.5*amount;
            break;
        case 2:
            preis=1*amount;
            break;
        case 3:
            preis=1.5*amount;
            break;
        case 4:
            preis=2*amount;
            break;
        default:
            printf("Invalid input please try again.\n");
            break;

    }

    return preis;

}
float paing(float preis) {

    printf("--- Bezahlvorgang ---\n");
    printf("--- Bezahlvorgang abbrechen mit 0 ---\n");
    float coin;
    float mreturn = 0;
    float origpreis = preis;
    int verify;

    do {
        printf("Der offen Betrag ist %f bitte Werfen Sie eine Muenze ein: \n", preis);
        scanf("%f", &coin);
        if(coin == 0){

            mreturn = origpreis-preis;
            printf("bitte entnehmen Sie ihr Wechselgeld von %f\n",mreturn);
            return 6;
        }
        verify = verifycoin(coin);
        if(verify == 1) {

            mreturn = 0;
            preis -= coin;

            if (preis < 0) {

                mreturn = preis * -1;
            }
        } else{
            printf("Ihr Geldstueck ist ungueltig.\n");

        }
    } while (preis > 0);
    return mreturn;
}


void output(unsigned int sorte, unsigned int amount, float mreturn){

    if(mreturn==6){

        return;
    }

    printf ("--- Getraenkeausgabe ---\n");

    for (int i = 1; i <= amount; ++i) {

        printf("Flasche %d von %d wurde ausgegeben(Sorte:%d)\n", i,amount,sorte);
        printf("\n");

    }
    printf ("--- Wechselgeldausgabe ---\n");
    printf("bitte entnehmen Sie ihr Wechselgeld von %f",mreturn);
}
int verifycoin(float coin){
    int money = coin*100;

    switch (money) {

        case 5:
            return 1;
        case 10:
            return 1;
        case 20:
            return 1;
        case 50:
            return 1;
        case 100:
            return 1;
        case 200:
            return 1;
        case 500:
            return 1;
        default:
            return 0;
    }
}